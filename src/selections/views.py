from django.views.generic import ListView, DetailView

from categories.models import Category
from .models import Collection


class CollectionListView(ListView):
    template_name = "selections/list.html"

    def get_queryset(self, *args, **kwargs):
        request = self.request
        return Collection.objects.all()


class CollectionDetailSlugView(DetailView):
    queryset = Collection.objects.all()
    template_name = "selections/detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super(CollectionDetailSlugView, self).get_context_data(*args, **kwargs)
        qs = self.get_object().primary_collection.all()
        categories    = Category.objects.all()
        collections   = Collection.objects.all()
        context['categories']  = categories
        context['collections'] = collections
        context['products'] = qs
        return context
