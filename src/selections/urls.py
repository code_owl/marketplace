from django.conf.urls import url


from .views import (
    CollectionListView,
    CollectionDetailSlugView,
    )

urlpatterns = [
    url(r'^$', CollectionListView.as_view(), name='list'),
    url(r'^(?P<slug>[\w-]+)/$', CollectionDetailSlugView.as_view(), name='detail'),
]
