from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from django.views.generic.base import RedirectView
from django.views.generic.edit import FormMixin

from products.models import Product

from .forms import NewSellerForm
from .mixins import SellerAccountMixin
from .models import SellerAccount


class SellerDashboard(SellerAccountMixin, FormMixin, View):
    queryset = SellerAccount.objects.all()
    form_class = NewSellerForm
    success_url ="/seller/"

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        apply_form = self.get_form()
        account = self.get_account()
        exists = account
        active = None
        context = {}
        if exists:
            active = account.active
        if not exists and not active:
            context["title"] = "Apply for account"
            context["apply_form"] = apply_form
        elif exists and not active:
            context["title"] = "Account pending"
        elif exists and active:
            context["title"] = "Seller Dashboard"
            context["products"] = self.get_products()
        else:
            pass
        return render(request, "sellers/dashboard.html", context)

    def form_valid(self, form):
        valid_data = super(SellerDashboard, self).form_valid(form)
        obj = SellerAccount.objects.create(user=self.request.user)
        return valid_data
