from django.conf.urls import url

from products.views import (
    ProductCreateView,
    SellerProductListView,
    ProductUpdateView
    )

from .views import SellerDashboard

urlpatterns = [
    url(r'^$', SellerDashboard.as_view(), name='dashboard'),
    url(r'^products/$', SellerProductListView.as_view(), name='product_list'),
    url(r'^products/add/$', ProductCreateView.as_view(), name='product_add'),
    url(r'^products/(?P<slug>[\w-]+)/edit/$', ProductUpdateView.as_view(), name='products_edit'),
]
