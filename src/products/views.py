from django.http import Http404
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.shortcuts import render, get_object_or_404

from carts.models import Cart
from categories.models import Category, Subcategory
from marketplace.forms import ProductAddForm, ProductModelForm
from marketplace.mixins import (
    LoginRequieredMixin,
    StaffRequieredMixin,
    MultiSlugMixin,
    SubmitButtonMixin
    )
from selections.models import Collection
from sellers.models import SellerAccount
from sellers.mixins import SellerAccountMixin

from .models import Product
from .mixins import ProductSellerMixin


class ProductCreateView(SellerAccountMixin, SubmitButtonMixin, CreateView):
    model = Product
    template_name = "form.html"
    form_class = ProductModelForm
    success_url = "/products/"
    submit_btn = "Add product"

    def form_valid(self, form):
        seller = self.get_account()
        form.instance.seller = seller
        valid_data = super(ProductCreateView, self).form_valid(form)
        return valid_data


class ProductUpdateView(SubmitButtonMixin, MultiSlugMixin, UpdateView):
    model = Product
    template_name = "form.html"
    form_class = ProductModelForm
    success_url = "/products/"
    submit_btn = "Update product"


class SellerProductListView(SellerAccountMixin, ListView):
    model = Product
    template_name = "sellers/product-list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(SellerProductListView, self).get_queryset(*args, **kwargs)
        queryset = qs.filter(seller=self.get_account())
        return queryset


class ProductFeaturedListView(ListView):
    queryset      = Product.objects.featured()
    template_name = "products/featured-list.html"

    def get_queryset(self, *args, **kwargs):
        request = self.request
        return Product.objects.all().featured()


class ProductFeaturedDetailView(DetailView):
    queryset      = Product.objects.all().featured()
    template_name = "products/featured-detail.html"


class ProductListView(ListView):
    queryset      = Product.objects.all()
    template_name = "products/list.html"

    def get_context_data(self, *args, **kwargs):
        context       = super(ProductListView, self).get_context_data(*args, **kwargs)
        categories    = Category.objects.all()
        collections   = Collection.objects.all()
        context['categories']  = categories
        context['collections'] = collections
        return context


class ProductDetailSlugView(MultiSlugMixin, DetailView):
    queryset      = Product.objects.all()
    template_name = "products/detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductDetailSlugView, self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        subcategories = Subcategory.objects.all().prefetch_related('primary_subcategory')
        collections   = Collection.objects.all().order_by("?")[:3]
        seller        = SellerAccount.objects.all()
        context['cart'] = cart_obj
        context['subcategories'] = subcategories
        context['collections'] = collections
        context['seller'] = seller
        return context


class ProductDetailView(DetailView):
    queryset      = Product.objects.all()
    template_name = "products/detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductDetailView, self).get_context_data(*args, **kwargs)
        print(context)
        return context

    def get_object(self, *args, **kwargs):
        request = self.request
        pk = self.kwargs.get('pk')
        instance = Product.objects.get_by_id(pk)
        if instance is None:
            raise Http404("Product doesn't exist")
        return instance
