from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from .models import Product, Image, Video


class VideolInLine(admin.TabularInline):
    extra = 1
    model = Video


class ImagelInLine(admin.TabularInline):
    extra = 1
    model = Image


class ProductAdmin(SummernoteModelAdmin):
    inlines = [
        ImagelInLine,
        VideolInLine
        ]
    list_display = ['__str__', 'slug']
    search_fields = ['title']
    summernote_fields = ('overview', 'specs', 'full_description')

    class Meta:
        model = Product

admin.site.register(Product, ProductAdmin)
