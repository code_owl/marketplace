from django.contrib import admin

from .models import MainPage, AboutPage, ContactPage


class MainPageAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'title_first']

    class Meta:
        model = MainPage


class AboutPageAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'title']

    class Meta:
        model = AboutPage


class ContactPageAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'title']

    class Meta:
        model = ContactPage

admin.site.register(MainPage, MainPageAdmin)

admin.site.register(AboutPage, AboutPageAdmin)

admin.site.register(ContactPage, ContactPageAdmin)
