from django.conf.urls import url

from .views import MainPageListView

urlpatterns = [
    url(r'^$', MainPageListView.as_view(), name='list'),
]
