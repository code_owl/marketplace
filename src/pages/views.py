from django.shortcuts import render
from django.views.generic import DetailView, ListView

from .models import MainPage, AboutPage, ContactPage


class MainPageListView(ListView):
    queryset = MainPage.objects.all()
    template_name = "pages/snippets/main-page-snippet.html"

class AboutPageListView(ListView):
    queryset = AboutPage.objects.all()
    template_name = "pages/snippets/about-page-snippet.html"
