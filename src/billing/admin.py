from django.contrib import admin

from .models import BillingProfile, Transaction

admin.site.register(BillingProfile)

admin.site.register(Transaction)
