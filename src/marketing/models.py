import random
import os

from django.db import models
from django.conf import settings
from django.db.models.signals import post_save, pre_save
from .utils import Mailchimp


def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext


def upload_image_path(instance, filename):
    new_filename = random.randint(1, 100000000000000000000)
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
    return "marketing/{new_filename}/{final_filename}".format(
        new_filename=new_filename,
        final_filename=final_filename
        )


class MarketingQueryset(models.query.QuerySet):
    def active(self):
        return self.filter(active=True)


class MarketingManager(models.Manager):
    def get_queryset(self):
        return MarketingQueryset(self.model, using=self._db)

    def all(self):
        return self.get_queryset().active()


class Slider(models.Model):
    title       = models.CharField(max_length=120, verbose_name="Slider title")
    url_product = models.URLField(verbose_name="Product URL", blank=True, null=True)
    image       = models.ImageField(upload_to=upload_image_path, null=True, blank=True, verbose_name="Slider image")
    order       = models.IntegerField(default=0)
    active      = models.BooleanField(default=False)

    objects = MarketingManager()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name        = "Slider"
        verbose_name_plural = "Sliders"


class Signup(models.Model):
    email = models.EmailField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name        = "Signup"
        verbose_name_plural = "Signups"


class MarketingPreference(models.Model):
    user                 = models.OneToOneField(settings.AUTH_USER_MODEL)
    subscribed           = models.BooleanField(default=True)
    mailchimp_subscribed = models.NullBooleanField(blank=True)
    mailchimp_msg        = models.TextField(null=True, blank=True)
    timestamp            = models.DateTimeField(auto_now_add=True)
    update               = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.email


def marketing_pref_create_receiver(sender, instance, created, *args, **kwargs):
    if created:
        status_code, response_data = Mailchimp().subscribe(instance.user.email)
        print(status_code, response_data)

post_save.connect(marketing_pref_create_receiver, sender=MarketingPreference)


def marketing_pref_update_receiver(sender, instance, *args, **kwargs):
    if instance.subscribed != instance.mailchimp_subscribed:
        if instance.subscribed:
            status_code, response_data = Mailchimp().subscribe(instance.user.email)
        else:
            status_code, response_data = Mailchimp().unsubscribe(instance.user.email)

        if response_data['status'] == "subscribed":
            instance.subscribed = True
            instance.mailchimp_subscribed = True
            instance.mailchimp_msg = response_data
        else:
            instance.subscribe = False
            instance.mailchimp_subscribed = False
            instance.mailchimp_msg = response_data

pre_save.connect(marketing_pref_update_receiver, sender=MarketingPreference)


def make_marketing_pref_receiver(sender, instance, created, *args, **kwargs):
    if created:
        MarketingPreference.objects.get_or_create(user=instance)

post_save.connect(make_marketing_pref_receiver, sender=settings.AUTH_USER_MODEL)
