from django.contrib import admin

from .models import Slider, Signup, MarketingPreference


class SliderAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'title']
    class Meta:
        model = Slider


class SignupAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'email']
    class Meta:
        model = Signup


class MarketingPreferenceAdmin(admin.ModelAdmin):
    readonly_fields = ['mailchimp_subscribed', 'timestamp', 'update']
    class Meta:
        model = MarketingPreference
        fields = ['user', 'subscribed', 'mailchimp_subscribed', 'timestamp', 'update']


admin.site.register(MarketingPreference, MarketingPreferenceAdmin)
admin.site.register(Signup, SignupAdmin)
admin.site.register(Slider)
