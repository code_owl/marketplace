from django.conf.urls import url


from .views import (
    CategoryListView,
    CategoryDetailSlugView,
    )

urlpatterns = [
    url(r'^$', CategoryListView.as_view(), name='list'),
    url(r'^(?P<slug>[\w-]+)/$', CategoryDetailSlugView.as_view(), name='detail'),
]
