import random
import os
from django.db import models
from django.db.models.signals import pre_save

from marketplace.utils import unique_slug_generator


def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext


def upload_image_path(instance, filename):
    new_filename = random.randint(1, 1000000000000)
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
    return "categories/{new_filename}/{final_filename}".format(
        new_filename=new_filename,
        final_filename=final_filename
        )


class CategoryQuerySet(models.query.QuerySet):
    def active(self):
        return self.filter(active=True)


class CategoryManager(models.Manager):
    def all(self):
        return self.get_queryset().all().active().prefetch_related('primary_category').prefetch_related('parent_category')

    def get_queryset(self):
        return CategoryQuerySet(self.model, using=self._db)

    def get_by_id(self, id):
        qs = self.get_queryset().filter(id=id)
        if qs.count() == 1:
            return qs.first()
        return None


class Category(models.Model):
    title             = models.CharField(max_length=300, verbose_name="Product category")
    slug              = models.SlugField(null=True, blank=True, verbose_name="Category URL", unique=True)
    short_description = models.CharField(blank=True, max_length=400, verbose_name="Category short description")
    full_description  = models.TextField(null=True, blank=True, verbose_name="Category full description")
    seo_title         = models.CharField(null=True, blank=True, max_length=120, verbose_name="Category SEO title")
    seo_text          = models.TextField(null=True, blank=True, verbose_name="Category SEO text")
    seo_description   = models.CharField(max_length=350, blank=True, null=True, verbose_name="Category SEO")
    position          = models.IntegerField(null=True, blank=True, verbose_name="Category position")
    image             = models.ImageField(upload_to=upload_image_path, null=True, blank=True, verbose_name="Category photo")
    icon              = models.FileField(null=True, blank=True, upload_to=upload_image_path)
    active            = models.BooleanField(default=True, verbose_name="Category active")

    objects = CategoryManager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return "/categories/{slug}/".format(slug=self.slug)

    class Meta:
        db_table            = 'categories'
        verbose_name        = "Product category"
        verbose_name_plural = "Product categories"
        ordering = ['position']

def category_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(category_pre_save_receiver, sender=Category)


class SubcategoryQuerySet(models.query.QuerySet):
    def active(self):
        return self.filter(active=True)


class SubcategoryManager(models.Manager):
    def all(self):
        return self.get_queryset().all().active().prefetch_related('primary_subcategory')

    def get_queryset(self):
        return SubcategoryQuerySet(self.model, using=self._db)

    def get_by_id(self, id):
        qs = self.get_queryset().filter(id=id)
        if qs.count() == 1:
            return qs.first()
        return None


class Subcategory(models.Model):
    title           = models.CharField(max_length=300, verbose_name="Product subcategory")
    category        = models.ForeignKey(Category,
            on_delete=models.CASCADE,
            null=True,
            related_name="parent_category"
        )
    slug            = models.SlugField(null=True, blank=True, verbose_name="Subcategory URL", unique=True)
    position        = models.IntegerField(null=True, blank=True, verbose_name="Subcategory position")
    description     = models.TextField(verbose_name="Subcategory description", blank=True, null=True)
    seo_title       = models.CharField(null=True, blank=True, max_length=120, verbose_name="Subcategory SEO title")
    seo_text        = models.TextField(null=True, blank=True, verbose_name="Subcategory SEO text")
    seo_description = models.CharField(max_length=350, blank=True, null=True, verbose_name="Subcategory SEO")
    image           = models.ImageField(upload_to=upload_image_path, null=True, blank=True, verbose_name="Subcategory image")
    active          = models.BooleanField(default=True, verbose_name="Subcategory active")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return "/subcategories/{slug}/".format(slug=self.slug)

    class Meta:
        verbose_name        = "Product subcategory"
        verbose_name_plural = "Product subcategories"
        ordering = ['position']


def subcategory_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(subcategory_pre_save_receiver, sender=Subcategory)
