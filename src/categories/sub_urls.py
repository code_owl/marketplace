from django.conf.urls import url


from .views import (
    SubcategoryListView,
    SubcategoryDetailSlugView,
    )

urlpatterns = [
    url(r'^$', SubcategoryListView.as_view(), name='list'),
    url(r'^(?P<slug>[\w-]+)/$', SubcategoryDetailSlugView.as_view(), name='detail'),
]
