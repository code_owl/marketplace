from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from .models import Category, Subcategory


class CategoryAdmin(SummernoteModelAdmin):
    list_display = ['__str__', 'title', 'slug']
    summernote_fields = ('seo_text')

    class Meta:
        model = Category


class SubcategoryAdmin(SummernoteModelAdmin):
    list_display = ['__str__', 'title']
    search_fields = ['title']
    summernote_fields = ('seo_text')

    class Meta:
        model = Subcategory

admin.site.register(Category, CategoryAdmin)
admin.site.register(Subcategory, SubcategoryAdmin)
